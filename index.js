var alexa = require('alexa-app');
var request  = require('request');
var _ = require('lodash');

// Allow this module to be reloaded by hotswap when changed
module.change_code = 1;

// Define an alexa-app
var app = new alexa.app('bookabooth');
app.launch(function(req,res) {
  res.say("Hello World!!");
});
app.intent('AvailabilityIntent', {
    "slots":{"ROOMNAME":"LITERAL"}
    ,"utterances":["{Is|are} the {purple line|purple room|buzz's office|office|ROOMNAME} available"]
  },function(req,res) {
// 		res.say('Your name is '+req.slot('NAME')+' and you are '+req.slot('AGE')+' years old');
    request('http://slalom-hack.herokuapp.com/api/rooms', function(error, response, body){
      if(error){
        res.say("Got an error");
        res.send();
        console.log("error= ", error);
        return;
      }
      var rooms = JSON.parse(body);

      if(!rooms || !rooms.length){
        res.say("We did not get a response from the book a booth api")
        res.send();
        console.log("We did not get a response from the book a booth api");
        return;
      }
      var roomName = req.slot('ROOMNAME');

      if(!roomName){
        res.say("We did not get your room name, please try again");
        res.send();
        console.log("We did not get your room name, please try again");

        return;
      }

      console.log("roomname=", roomName);

      var room = _.find(rooms, function(o){
        return o.name.toLowerCase() === roomName.toLowerCase();
      });


      console.log("room= ", room)

      if(!room){
        res.say("We did not find a room with that name");
        console.log("We did not find a room with that name");
        res.send();
        return;
      }

      if(room.is_booked) {
        res.say("The room is currently booked");
        console.log("The room is currently booked");
        res.send();
        return;
      }

      if(room.is_holding) {
        res.say("Nobody is in the room, but it is currently being held");
        console.log("Nobody is in the room, but it is currently being held");
        res.send();
        return;
      }

      res.say("The room is available");
      console.log("The room is available");
      res.send();
      return;
    });
    return false;
  }
);
app.intent('AnyAvailableIntent', {
    "utterances":["{is|are} there any {room|rooms|booth|booths} available"]
  },function(req,res) {

    request('http://slalom-hack.herokuapp.com/api/rooms', function(error, response, body){
      var rooms = JSON.parse(body);

      if(!rooms || !rooms.length){
        res.say("We did not get a response from the book a booth api")
        res.send();
        return;
      }

      var availableRoom = _.find(rooms, {is_booked: false, is_holding: false});
      if(!availableRoom){
        res.say("There are no rooms available to book");
        res.send();
        return;
      }

      res.say("The " + availableRoom.name +  "is available to book;");
      res.send();
      return;

    });
    return false;
  }
);
exports.handler = app.lambda();

